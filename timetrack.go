package timetrack

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	timeTrack = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "track_time",
		Help: "Counts the total amount of time spent on tracked activities",
	}, []string{
		"activity",
	})
	timeCount = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "track_count",
		Help: "Counts the total invocations on tracked activities",
	}, []string{
		"activity",
	})
)
