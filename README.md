# TimeTrack

`go get gitlab.com/T4cC0re/time-track` and  `import "gitlab.com/T4cC0re/time-track"` as usual

Simple example usage:
```go
func demo() {
	defer timetrack.TimeTrack(time.Now())
	fmt.Println("foobar")
	time.Sleep(time.Second)
}
```
Output: ("foobar" is on stdout, the rest on stderr)
```
foobar
time="2019-01-29T20:36:42+01:00" level=info msg="main.demo | <nil> | 1.000139453s" source="timetrack.go:46"
```

See, very easy.

This also exports prometheus metrics automagically (if you set up a prometheus metrics server in your app, e.g. via `promhttp.Handler()`)
```
# HELP track_count Counts the total invocations on tracked activities
# TYPE track_count counter
track_count{activity="main.demo"} 1
# HELP track_time Counts the total amount of time spent on tracked activities
# TYPE track_time counter
track_time{activity="main.demo"} 1.000148e+06
```