// +build log

package timetrack

import (
	"github.com/prometheus/common/log"
	"time"
)

func TimeTrack(start time.Time) {
	elapsed, name := timeTracF(start, "")
	log.Infof("%s | <nil> | %s", name, elapsed)
}
func TimeTrackCustom(start time.Time, name string) {
	elapsed, name := timeTracF(start, name)
	log.Infof("%s | <nil> | %s", name, elapsed)
}

func TimeTrackX(start time.Time, args ...interface{}) {
	elapsed, name := timeTracF(start, "")

	if format, ok := args[0].(string); ok {
		args[0] = name
		args = append(args, elapsed/time.Millisecond)
		log.Infof(format, args...)
		return
	}
	log.Infof("%s | %+v | %s", name, args, elapsed/time.Microsecond)
}
