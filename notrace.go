// +build !trace

package timetrack

import (
	"path"
	"runtime"
	"time"
)

func timeTracF(start time.Time, name string) (time.Duration, string) {
	elapsed := time.Since(start)
	if name == "" {
		pc, _, _, _ := runtime.Caller(2)
		details := runtime.FuncForPC(pc)
		name = path.Base(details.Name())
	}
	timeTrack.WithLabelValues(name).Add(float64(elapsed / time.Microsecond))
	timeCount.WithLabelValues(name).Inc()
	return elapsed, name
}
