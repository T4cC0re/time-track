// +build !log

package timetrack

import (
	"time"
)

func TimeTrack(start time.Time) {
	timeTracF(start, "")
}
func TimeTrackCustom(start time.Time, name string) {
	timeTracF(start, name)
}

func TimeTrackX(start time.Time, args ...interface{}) {
	timeTracF(start, "")
}
