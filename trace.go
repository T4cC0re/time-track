// +build trace

package timetrack

import (
	"github.com/prometheus/common/log"
	"path"
	"runtime"
	"time"
)

func timeTracF(start time.Time, name string) (time.Duration, string) {
	elapsed := time.Since(start)
	pc, file, line, ok := runtime.Caller(2)
	details := runtime.FuncForPC(pc)
	if ok && details != nil {
		log.Debugf("called from %s (%s:%d)", path.Base(details.Name()), file, line)
	}
	if name == "" {
		name = path.Base(details.Name())
	}
	timeTrack.WithLabelValues(name).Add(float64(elapsed / time.Microsecond))
	timeCount.WithLabelValues(name).Inc()
	return elapsed, name
}
